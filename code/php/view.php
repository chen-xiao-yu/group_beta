<?php
	$x = $_GET['Name'];
	include("conn.php");
	$stmt = $db->prepare("select * from file where name=?");
	$stmt->execute(array($x));
	$result = $stmt->fetch(PDO::FETCH_ASSOC);

	if($result['type']=="资料"){
		$type = strrchr($result['name'],".");
		$address = $result['address'];
		if($type==".txt"){
			echo "<a href=\"".$result['address']."\" download=\"".$result['address']."\">下载</a>";
			echo "</br>";
			$handle = fopen($address, 'r');
		    while(!feof($handle)){
		        echo fgets($handle, 1024);
		        echo "</br>";
		    }
		    fclose($handle);
		}
		else if($type==".png" || $type==".jpg" || $type==".bmp" || $type==".gif"){
			$url = $address; 
			$img = file_get_contents($url,true); 
			header("Content-Type: image/jpeg;text/html; charset=utf-8"); 
			echo $img; 
			exit;
		}
		else if($type==".doc" || $type==".docx" || $type==".ppt" || $type==".xlsx" || $type==".xls" || $type==".pptx"){
			echo "<iframe style='width: 100%;min-height: 1000px;' src='https://api.idocv.com/view/url?url=http://121.5.53.194/存储文件/".$result['name']."'  frameborder='1'>";
		}
		else if($type==".pdf"){
			echo "<embed src=\"".$address."\" type=\"application/pdf\" style=\"overflow: auto; position: absolute; top: 0; right: 0; bottom: 0; left: 0; width: 100%; height: 100%;\">";
		}
		else if($type==".mp4" || $type==".avi" || $type==".mkv" || $type==".mkv" || $type==".mkv" || $type==".rmvb"){
			echo "<a href=\"".$result['address']."\" download=\"".$result['address']."\">下载</a>";
			echo "</br>";
			echo "<video src=\"".$address."\" controls=\"controls\" download=\"".$result['address']."\"></video>";
		}
		else{
			echo "<h1>抱歉，该类型文件暂不支持预览，请下载后查看</h1>";
			echo "</br>";
			echo "<a href=\"".$result['address']."\" download=\"".$result['address']."\">下载</a>";
		}
	}
	else{
		echo "<h1 align=\"center\">关于".$result['year']."年".$result['subject']."的小道消息</h1>";
		echo "<h3 align=\"center\">".$result['name']."<h3/>";
	}
?>